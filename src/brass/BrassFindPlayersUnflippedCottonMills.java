package brass;

import java.util.*;

class BrassFindPlayersUnflippedCottonMills implements util.FindCommand<BrassCity> {
	private List<Integer> player_unflipped_cotton_mills;
	private int player_id;

	public BrassFindPlayersUnflippedCottonMills(int player_id) {
		player_unflipped_cotton_mills = new ArrayList<Integer>();

		this.player_id = player_id;
	}

	
	public void execute(BrassCity brass_city) {
		util.CountCommand<BrassIndustry> player_cotton_mills = new BrassCountPlayersUnflippedIndustry(2, player_id);
		brass_city.execute(player_cotton_mills);

		for(int i=0; i < player_cotton_mills.getCount(); i++) {
			player_unflipped_cotton_mills.add(brass_city.getCityID());
		}
	}

	public List<Integer> getList() {
		return player_unflipped_cotton_mills;
	}
}