package brass;

import java.util.List;

abstract class BrassConnectionSearch implements BrassConnectionSearchInterface {
	protected List<BrassConnection> brass_connections;

	public BrassConnectionSearch(List<BrassConnection> brass_connections){
		this.brass_connections = brass_connections;
	}

	public abstract table.TableInterface<Integer, Integer> connectionSearch(int start_city_id, table.Comparator<Integer, Integer> comp_city_ids);

	protected int isLinkConnected(int city_id, int[] connected_cities) {
		int test_id = 0;
		
		if (connected_cities[0] == city_id)
			test_id = connected_cities[1];
		else if (connected_cities[1] == city_id)
			test_id = connected_cities[0];
		
		return test_id;
	}
}